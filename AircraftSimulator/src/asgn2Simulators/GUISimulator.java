/**
 * 
 * This file is part of the AircraftSimulator Project, written as 
 * part of the assessment for CAB302, semester 1, 2016. 
 * 
 */
package asgn2Simulators;
 
import java.awt.BorderLayout;
import java.awt.HeadlessException;
import java.awt.List;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;
 
/**
 * @author hogan
 *
 */
@SuppressWarnings("serial")
public class GUISimulator extends JFrame implements Runnable {
     
    private static final long serialVersionUID = 8645229832063987966L;
    public static final int WIDTH = 1000;
    public static final int HEIGHT = 600;
 
    private ButtonPanel buttonPanel;
    private SimulationInputPanel simInputPanel;
    private FareClassInputPanel fareClassInputPanel;
    private ChartPanel chartPanel;
    private LegendPanel legendPanel;
     
    /**
     * @param arg0
     * @throws HeadlessException
     */
    public GUISimulator(String arg0) throws HeadlessException {
        super(arg0);
    }
 
    public void createGUI() {
        setSize(WIDTH, HEIGHT);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
 
        buttonPanel = new ButtonPanel();
        simInputPanel = buttonPanel.SIP;
        fareClassInputPanel = buttonPanel.FCIP;
        chartPanel = new ChartPanel();
        legendPanel = new LegendPanel();
         
        buttonPanel.setInputListener(new InputPanelsListener() {
            public void inputEventOccurred(InputPanelsEvent e) {
                String RNG_Seed = e.getRNG_Seed();
                String dailyMean = e.getDailyMean();
                String queueSize = e.getQueueSize();
                String cancellation = e.getCancellation();
                String first = e.getFirst();
                String business = e.getBusiness();
                String premium = e.getPremium();
                String economy = e.getEconomy();
             
                String logPath;
                
                //	Open the log file and read it
                try {
                    logPath = SimulationRunner.createAndRunSimulation(new String[]{ RNG_Seed, queueSize, dailyMean, 
                            Double.toString(Constants.DEFAULT_DAILY_BOOKING_SD), first, business, premium, 
                            economy, cancellation});
                } catch (Exception e1) {
                    e1.printStackTrace();
                    return;
                }
                
                // Make ArrayList to store each line of the Logfile
                ArrayList<String> list = new ArrayList<>();
                
                // Stream each line into the Logfile
                try (Stream<String> stream = Files.lines(Paths.get(logPath))) {
                	list = (ArrayList<String>) stream.collect(Collectors.toList());

        		} catch (IOException excep) {
        			excep.printStackTrace();
        		}
          
                // Send to Console
                for (String s : list) {
                	System.out.println(s);
                }
                
                // Send to "AppendText" area
                for (String s : list) {
                	chartPanel.appendText(s + "\n");
                }            
            }
        });
 
        this.add(buttonPanel, BorderLayout.SOUTH);
        this.add(simInputPanel, BorderLayout.WEST);
        this.add(fareClassInputPanel, BorderLayout.EAST);
        this.add(chartPanel, BorderLayout.NORTH);
        this.add(legendPanel, BorderLayout.CENTER);
         
    }
 
    /* (non-Javadoc)
     * @see java.lang.Runnable#run()
     */
    @Override
    public void run() { 
        createGUI();
    }
 
    /**
     * @param args
     */
    public static void main(String[] args) {
        JFrame.setDefaultLookAndFeelDecorated(true);
        SwingUtilities.invokeLater(new GUISimulator("Aircraft Bookings Simulator"));
    }
 
}