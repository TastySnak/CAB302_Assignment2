package asgn2Simulators;

import java.util.EventObject;

public class InputPanelsEvent extends EventObject{

	private String RNG_Seed;
	private String dailyMean;
	private String queueSize;
	private String cancellation;
	private String first;
	private String business;
	private String premium;
	private String economy;
	
	public InputPanelsEvent(Object source) {
		super(source);
	}

	public InputPanelsEvent(Object source, String RNG_Seed, String dailyMean, String queueSize, String cancellation, 
			String first, String business, String premium, String economy) {
		super(source);
		
		this.RNG_Seed = RNG_Seed;
		this.dailyMean = dailyMean;
		this.queueSize = queueSize;
		this.cancellation = cancellation;
		this.first = first;
		this.business = business;
		this.premium = premium;
		this.economy = economy;
	}

	public String getRNG_Seed() {
		return RNG_Seed;
	}
	
	public void setRNG_Seed(String RNG_Seed) {
		this.RNG_Seed = RNG_Seed;
	}
	
	public String getDailyMean() {
		return dailyMean;
	}
	
	public void setDailyMean(String dailyMean) {
		this.dailyMean = dailyMean;
	}
	
	public String getQueueSize() {
		return queueSize;
	}
	
	public void setQueueSize(String queueSize) {
		this.queueSize = queueSize;
	}
	
	public String getCancellation() {
		return cancellation;
	}
	
	public void setCancellation(String cancellation) {
		this.cancellation = cancellation;
	}

	public String getFirst() {
		return first;
	}
	
	public void setFirst(String first) {
		this.first = first;
	}

	public String getBusiness() {
		return business;
	}
	
	public void setBusiness(String business) {
		this.business = business;
	}

	public String getPremium() {
		return premium;
	}
	
	public void setPremium(String premium) {
		this.premium = premium;
	}

	public String getEconomy() {
		return economy;
	}
	
	public void setEconomy(String economy) {
		this.economy = economy;
	}
	
}
