package asgn2Simulators;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;

public class SimulationInputPanel extends JPanel{
	
	private static final long serialVersionUID = 8645229832063987966L;
	
	private JLabel RNG_SeedLabel;
	private JLabel dailyMeanLabel;
	private JLabel queueSizeLabel;
	private JLabel cancellationLabel;
	private JTextField RNG_SeedField;
	private JTextField dailyMeanField;
	private JTextField queueSizeField;
	private JTextField cancellationField;
	
	public SimulationInputPanel() {
		Dimension dim = getPreferredSize();
		dim.width = 300;
		setPreferredSize(dim);
		
		RNG_SeedLabel = new JLabel("RNG Seed");
		dailyMeanLabel = new JLabel("Daily Mean");
		queueSizeLabel = new JLabel("Queue Size");
		cancellationLabel = new JLabel("Cancellation");
		RNG_SeedField = new JTextField(Constants.DEFAULT_SEED + "", 10);  // 
		dailyMeanField = new JTextField(Constants.DEFAULT_DAILY_BOOKING_MEAN + "", 10);
		queueSizeField = new JTextField(Constants.DEFAULT_MAX_QUEUE_SIZE + "", 10);
		cancellationField = new JTextField(Constants.DEFAULT_CANCELLATION_PROB + "", 10);

		Border innerBorder = BorderFactory.createTitledBorder("Simulation Input");
		Border outerBorder = BorderFactory.createEmptyBorder(5, 5, 5, 5);
		setBorder(BorderFactory.createCompoundBorder(outerBorder, innerBorder));

		setLayout(new GridBagLayout());
		
		GridBagConstraints gc = new GridBagConstraints();
		
		///////////// RNG Seed ///////////////
		gc.weightx = 1;
		gc.weighty = 0.1;
		
		gc.gridx = 0;
		gc.gridy = 0;
		gc.fill = GridBagConstraints.NONE;
		gc.anchor = GridBagConstraints.LINE_END;
		gc.insets = new Insets(0, 0, 0, 5);
		add(RNG_SeedLabel, gc);
		
		gc.gridx = 1;
		gc.gridy = 0;
		gc.anchor = GridBagConstraints.LINE_START;
		gc.insets = new Insets(0, 0, 0, 0);
		add(RNG_SeedField, gc);
		
		///////////// Daily Mean ///////////////
		gc.weightx = 1;
		gc.weighty = 0.1;
		
		gc.gridx = 0;
		gc.gridy = 1;
		gc.anchor = GridBagConstraints.LINE_END;
		gc.insets = new Insets(0, 0, 0, 5);
		add(dailyMeanLabel, gc);
		
		gc.gridx = 1;
		gc.gridy = 1;
		gc.anchor = GridBagConstraints.LINE_START;
		gc.insets = new Insets(0, 0, 0, 0);
		add(dailyMeanField, gc);
		
		///////////// Queue Size ///////////////
		gc.weightx = 1;
		gc.weighty = 0.1;
		
		gc.gridx = 0;
		gc.gridy = 2;
		gc.anchor = GridBagConstraints.LINE_END;
		gc.insets = new Insets(0, 0, 0, 5);
		add(queueSizeLabel, gc);
		
		gc.gridx = 1;
		gc.gridy = 2;
		gc.anchor = GridBagConstraints.LINE_START;
		gc.insets = new Insets(0, 0, 0, 0);
		add(queueSizeField, gc);
		
		///////////// Cancellation ///////////////
		gc.weightx = 1;
		gc.weighty = 0.1;
		
		gc.gridx = 0;
		gc.gridy = 3;
		gc.anchor = GridBagConstraints.LINE_END;
		gc.insets = new Insets(0, 0, 0, 5);
		add(cancellationLabel, gc);
		
		gc.gridx = 1;
		gc.gridy = 3;
		gc.anchor = GridBagConstraints.LINE_START;
		gc.insets = new Insets(0, 0, 0, 0);
		add(cancellationField, gc);
		
		
	}
	
	// Getter methods to retrieve values from input fields
	public String getRNG_Seed() {
		return RNG_SeedField.getText();
	}
	
	public String getDailyMean() {
		return dailyMeanField.getText();
	}
	
	public String getQueueSize() {
		return queueSizeField.getText();
	}
	
	public String getCancellation() {
		return cancellationField.getText();
	}
}