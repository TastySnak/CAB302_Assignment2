package asgn2Simulators;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;

public class FareClassInputPanel extends JPanel{
	
	private static final long serialVersionUID = 8645229832063987966L;
		
	private JLabel firstLabel;
	private JLabel businessLabel;
	private JLabel premiumLabel;
	private JLabel economyLabel;
	private JTextField firstField;
	private JTextField businessField;
	private JTextField premiumField;
	private JTextField economyField;
	
	public FareClassInputPanel() {
		Dimension dim = getPreferredSize();
		dim.width = 300;
		setPreferredSize(dim);
		
		firstLabel = new JLabel("First");
		businessLabel = new JLabel("Business");
		premiumLabel = new JLabel("Premium");
		economyLabel = new JLabel("Economy");
		firstField = new JTextField(Constants.DEFAULT_FIRST_PROB + "", 10);
		businessField = new JTextField(Constants.DEFAULT_BUSINESS_PROB + "", 10);
		premiumField = new JTextField(Constants.DEFAULT_PREMIUM_PROB + "", 10);
		economyField = new JTextField(Constants.DEFAULT_ECONOMY_PROB + "", 10);

		Border innerBorder = BorderFactory.createTitledBorder("Fare Classes Input");
		Border outerBorder = BorderFactory.createEmptyBorder(5, 5, 5, 5);
		setBorder(BorderFactory.createCompoundBorder(outerBorder, innerBorder));

		setLayout(new GridBagLayout());
		
		GridBagConstraints gc = new GridBagConstraints();
		
		///////////// First ///////////////
		gc.weightx = 1;
		gc.weighty = 0.1;
		
		gc.gridx = 0;
		gc.gridy = 0;
		gc.fill = GridBagConstraints.NONE;
		gc.anchor = GridBagConstraints.LINE_END;
		gc.insets = new Insets(0, 0, 0, 5);
		add(firstLabel, gc);
		
		gc.gridx = 1;
		gc.gridy = 0;
		gc.anchor = GridBagConstraints.LINE_START;
		gc.insets = new Insets(0, 0, 0, 0);
		add(firstField, gc);
		
		///////////// Business ///////////////
		gc.weightx = 1;
		gc.weighty = 0.1;
		
		gc.gridx = 0;
		gc.gridy = 1;
		gc.anchor = GridBagConstraints.LINE_END;
		gc.insets = new Insets(0, 0, 0, 5);
		add(businessLabel, gc);
		
		gc.gridx = 1;
		gc.gridy = 1;
		gc.anchor = GridBagConstraints.LINE_START;
		gc.insets = new Insets(0, 0, 0, 0);
		add(businessField, gc);
		
		///////////// Premium ///////////////
		gc.weightx = 1;
		gc.weighty = 0.1;
		
		gc.gridx = 0;
		gc.gridy = 2;
		gc.anchor = GridBagConstraints.LINE_END;
		gc.insets = new Insets(0, 0, 0, 5);
		add(premiumLabel, gc);
		
		gc.gridx = 1;
		gc.gridy = 2;
		gc.anchor = GridBagConstraints.LINE_START;
		gc.insets = new Insets(0, 0, 0, 0);
		add(premiumField, gc);
		
		///////////// Economy ///////////////
		gc.weightx = 1;
		gc.weighty = 0.1;
		
		gc.gridx = 0;
		gc.gridy = 3;
		gc.anchor = GridBagConstraints.LINE_END;
		gc.insets = new Insets(0, 0, 0, 5);
		add(economyLabel, gc);
		
		gc.gridx = 1;
		gc.gridy = 3;
		gc.anchor = GridBagConstraints.LINE_START;
		gc.insets = new Insets(0, 0, 0, 0);
		add(economyField, gc);
	}
	
	
	// Getter methods to retrieve values from input fields
	public String getFirst() {
		return firstField.getText();
	}
	
	public String getBusiness() {
		return businessField.getText();
	}
	
	public String getPremium() {
		return premiumField.getText();
	}
	
	public String getEconomy() {
		return economyField.getText();
	}
}