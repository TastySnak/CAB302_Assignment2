package asgn2Simulators;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class ChartPanel extends JPanel {

	private static final long serialVersionUID = 8645229832063987966L;
	
	private JTextArea textArea;
	
	// Create the Text Area within the panel
	public ChartPanel() {
		Dimension dim = getPreferredSize();
		dim.height = 400;
		setPreferredSize(dim);
		
		textArea = new JTextArea(25, 1);
		setLayout(new BorderLayout());
		add(new JScrollPane(textArea), BorderLayout.NORTH);
	}

	public void appendText(String text) {
		textArea.append(text);
	}
}
