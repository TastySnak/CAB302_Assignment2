package asgn2Tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import asgn2Passengers.Business;
import asgn2Passengers.Economy;
import asgn2Passengers.First;
import asgn2Passengers.Passenger;
import asgn2Passengers.PassengerException;
import asgn2Passengers.Premium;

public class FirstTests {
	
	private Passenger firstPassenger;
	private Passenger businessPassenger;
	private Passenger premiumPassenger;
	private Passenger economyPassenger;
	
	private final int BOOKING_TIME = 2;
	private final int QUEUE_TIME = 3;
	private final int REFUSAL_TIME = 5;
	private final int CONFIRMATION_TIME = 7;
	private final int CANCELLATION_TIME = 8;
	private final int DEPARTURE_TIME = 10;
	
	@Before
	/*
	 * Create fresh passengers before each test
	 */
	public void createPassengers() throws PassengerException {
		firstPassenger = new First(BOOKING_TIME, DEPARTURE_TIME);
		businessPassenger = new Business(BOOKING_TIME, DEPARTURE_TIME);
		premiumPassenger = new Premium(BOOKING_TIME, DEPARTURE_TIME);
		economyPassenger = new Economy(BOOKING_TIME, DEPARTURE_TIME);
	}

	/////////////// Constructor Tests /////////////////////
	
	@Test
	/*
	 * Testing the constructor by creating a new passenger
	 */
	public void createNewPassenger() throws PassengerException {
		Passenger aPassenger = new First(2, 10);
		assertEquals(2, aPassenger.getBookingTime());
	}
	
	@Test (expected = PassengerException.class)
	/*
	 * Testing the constructor by entering an invalid negative value booking time
	 */
	public void invalidBookingTimeInConstructor() throws PassengerException {
		@SuppressWarnings("unused")
		Passenger aPassenger = new First(-1, 10);
	}
	
	@Test (expected = PassengerException.class)
	/*
	 * Testing the constructor by entering an invalid zero value departure time
	 */
	public void invalidDepartureTimeIsZeroInConstructor() throws PassengerException {
		@SuppressWarnings("unused")
		Passenger aPassenger = new First(2, 0);
	}
	
	@Test (expected = PassengerException.class)
	/*
	 * Testing the constructor by entering an invalid negative value departure time
	 */
	public void invalidDepartureTimeBelowZeroInConstructor() throws PassengerException {
		@SuppressWarnings("unused")
		Passenger aPassenger = new First(2, -2);
	}
	
	@Test (expected = PassengerException.class)
	/*
	 * Testing the constructor by entering a booking time greater than the departure time
	 */
	public void departureTimeBeforeBookingTimeInConstructor() throws PassengerException {
		@SuppressWarnings("unused")
		Passenger aPassenger = new First(10, 2);
	}
	
	@Test 
	/*
	 * Testing the constructor by entering a valid boundary case for booking time
	 */
	public void BookingTimeBoundaryInConstructor() throws PassengerException {
		Passenger aPassenger = new First(0, 10);
		assertEquals(0, aPassenger.getBookingTime());
	}
	
	@Test
	/*
	 * Testing the constructor by entering a valid boundary case for departure time
	 */
	public void DepartureTimeBoundaryInConstructor() throws PassengerException {
		Passenger aPassenger = new First(0, 1);
		assertEquals(1, aPassenger.getDepartureTime());
	}
	
	///////////////// cancelSeat() Tests //////////////////////
	
	@Test
	/*
	 * Testing cancelSeat() method to make sure it successfully cancels a passenger
	 */
	public void cancelSeatConfirmedPassenger() throws PassengerException {
		firstPassenger.confirmSeat(CONFIRMATION_TIME, DEPARTURE_TIME);
		firstPassenger.cancelSeat(CANCELLATION_TIME);
		assertTrue(firstPassenger.isNew());
	}
	
	@Test (expected = PassengerException.class)
	/*
	 * Testing the cancelSeat() method to make sure it throws an exception
	 * when used on a new Passenger
	 */
	public void cancelSeatNewPassenger() throws PassengerException {
		firstPassenger.cancelSeat(5);
	}
	
	@Test (expected = PassengerException.class)
	/*
	 * Testing the cancelSeat() method to make sure it throws an exception
	 * when used on a queued passenger
	 */
	public void cancelSeatQueuedPassenger() throws PassengerException {
		firstPassenger.queuePassenger(QUEUE_TIME, DEPARTURE_TIME);
		firstPassenger.cancelSeat(CANCELLATION_TIME);
	}
	
	@Test (expected = PassengerException.class)
	/*
	 * Testing the cancelSeat() method to make sure it throws an exception
	 * when used on a refused passenger
	 */
	public void cancelSeatRefusedPassenger() throws PassengerException {
		firstPassenger.refusePassenger(REFUSAL_TIME);
		firstPassenger.cancelSeat(CANCELLATION_TIME);
	}
	
	@Test (expected = PassengerException.class)
	/*
	 * Testing the cancelSeat() method to make sure it throws an exception
	 * when used on a flown passenger
	 */
	public void cancelSeatFlownPassenger() throws PassengerException {
		firstPassenger.confirmSeat(CONFIRMATION_TIME, DEPARTURE_TIME);
		firstPassenger.flyPassenger(DEPARTURE_TIME);
		firstPassenger.cancelSeat(DEPARTURE_TIME);
	}
	
	@Test (expected = PassengerException.class)
	/*
	 * Testing the cancelSeat() method to make sure it throws an exception
	 * an invalid negative cancellation time is provided
	 */
	public void cancelSeatInvalidCancellationTime() throws PassengerException {
		firstPassenger.confirmSeat(CONFIRMATION_TIME, DEPARTURE_TIME);
		firstPassenger.cancelSeat(-5);
	}
	
	@Test (expected = PassengerException.class)
	/*
	 * Testing the cancelSeat() method to make sure it throws an exception
	 * when the cancellation time is greater than the departure time
	 */
	public void cancelSeatDepTimeBeforeCancelTime() throws PassengerException {
		firstPassenger.confirmSeat(CONFIRMATION_TIME, DEPARTURE_TIME);
		firstPassenger.cancelSeat(DEPARTURE_TIME + 2);
	}

	////////////// confirmSeat() Tests //////////////////

	@Test
	/*
	 * Testing confirmSeat() method to make sure a 
	 * new passenger's seat is able to be confirmed
	 */
	public void confirmSeatNewPassenger() throws PassengerException {
		firstPassenger.confirmSeat(CONFIRMATION_TIME, DEPARTURE_TIME);
		assertTrue(firstPassenger.isConfirmed());
	}

	@Test
	/*
	 * Testing confirmSeat() method to make sure a 
	 * queued passenger's seat is able to be confirmed
	 */
	public void confirmSeatQueuedPassenger() throws PassengerException {
		firstPassenger.queuePassenger(QUEUE_TIME, DEPARTURE_TIME);
		firstPassenger.confirmSeat(CONFIRMATION_TIME, DEPARTURE_TIME);
		assertTrue(firstPassenger.isConfirmed());
	}
	
	@Test (expected = PassengerException.class)
	/*
	 * Testing confirmSeat() method to make sure it throws an exception
	 * when used on an already confirmed passenger
	 */
	public void confirmSeatConfirmedPassenger() throws PassengerException {
		firstPassenger.confirmSeat(CONFIRMATION_TIME, DEPARTURE_TIME);
		firstPassenger.confirmSeat(CONFIRMATION_TIME, DEPARTURE_TIME);
	}
	
	@Test (expected = PassengerException.class)
	/*
	 * Testing confirmSeat() method to make sure it throws an exception
	 * when used on an already refused passenger
	 */
	public void confirmSeatRefusedPassenger() throws PassengerException {
		firstPassenger.refusePassenger(REFUSAL_TIME);
		firstPassenger.confirmSeat(CONFIRMATION_TIME, DEPARTURE_TIME);
	}
	
	@Test (expected = PassengerException.class)
	/*
	 * Testing confirmSeat() method to make sure it throws an exception
	 * when used on an already flown passenger
	 */
	public void confirmSeatFlownPassenger() throws PassengerException {
		firstPassenger.confirmSeat(CONFIRMATION_TIME, DEPARTURE_TIME);
		firstPassenger.flyPassenger(DEPARTURE_TIME);
		firstPassenger.confirmSeat(CONFIRMATION_TIME, DEPARTURE_TIME);
	}
	
	@Test (expected = PassengerException.class)
	/*
	 * Testing confirmSeat() method to make sure it throws an exception
	 * when invalid negative confirmation time entered
	 */
	public void confirmSeatInvalidConfirmationTime() throws PassengerException {
		firstPassenger.confirmSeat(-5, DEPARTURE_TIME);
	}

	@Test (expected = PassengerException.class)
	/*
	 * Testing confirmSeat() method to make sure it throws an exception
	 * when the departure time is less than the confirmation time
	 */
	public void confirmSeatDepartureTimeLessThanConfirmationTime() throws PassengerException {
		firstPassenger.confirmSeat(12, DEPARTURE_TIME);
	}

	/////////////////// flyPassenger() Tests /////////////////
	
	@Test
	/*
	 * Testing flyPassenger() method to make sure it transitions 
	 * a confirmed passenger to flown
	 */
	public void flyPassengerConfirmedPassenger() throws PassengerException {
		firstPassenger.confirmSeat(CONFIRMATION_TIME, DEPARTURE_TIME);
		firstPassenger.flyPassenger(DEPARTURE_TIME);
		assertTrue(firstPassenger.isFlown());
	}
	
	@Test (expected = PassengerException.class)
	/*
	 * Testing flyPassenger() method to make sure it throws an exception
	 * when used on a new passenger
	 */
	public void flyPassengerNewPassenger() throws PassengerException {
		firstPassenger.flyPassenger(DEPARTURE_TIME);
	}

	@Test (expected = PassengerException.class)
	/*
	 * Testing flyPassenger() method to make sure it throws an exception
	 * when used on a queued passenger
	 */
	public void flyPassengerQueuedPassenger() throws PassengerException {
		firstPassenger.queuePassenger(QUEUE_TIME, DEPARTURE_TIME);
		firstPassenger.flyPassenger(DEPARTURE_TIME);
	}

	@Test (expected = PassengerException.class)
	/*
	 * Testing flyPassenger() method to make sure it throws an exception
	 * when used on a refused passenger
	 */
	public void flyPassenger() throws PassengerException {
		firstPassenger.refusePassenger(REFUSAL_TIME);
		firstPassenger.flyPassenger(DEPARTURE_TIME);
	}

	@Test (expected = PassengerException.class)
	/*
	 * Testing flyPassenger() method to make sure it throws an exception
	 * when used on an already flown passenger
	 */
	public void flyPassengerFlownPassenger() throws PassengerException {
		firstPassenger.confirmSeat(CONFIRMATION_TIME, DEPARTURE_TIME);
		firstPassenger.flyPassenger(DEPARTURE_TIME);
		firstPassenger.flyPassenger(DEPARTURE_TIME);
	}

	@Test (expected = PassengerException.class)
	/*
	 * Testing flyPassenger() method to make sure it throws an exception
	 * when used on a new passenger
	 */
	public void flyPassengerInvalidDepartureTimeIsZero() throws PassengerException {
		firstPassenger.confirmSeat(CONFIRMATION_TIME, DEPARTURE_TIME);
		firstPassenger.flyPassenger(0);
	}

	@Test (expected = PassengerException.class)
	/*
	 * Testing flyPassenger() method to make sure it throws an exception
	 * when used on a new passenger
	 */
	public void flyPassengerInvalidDepartureTimeBelowZero() throws PassengerException {
		firstPassenger.confirmSeat(CONFIRMATION_TIME, DEPARTURE_TIME);
		firstPassenger.flyPassenger(-5);
	}
	
	////////////////// queuePassenger() Tests //////////////
	
	@Test
	/*
	 * Test the queuePassenger() method to make sure it successfully 
	 * transfers a new passenger to a queued passenger
	 */
	public void queuePassengerNewPassenger() throws PassengerException {
		firstPassenger.queuePassenger(QUEUE_TIME, DEPARTURE_TIME);
		assertTrue(firstPassenger.isQueued());
	}
	
	@Test (expected = PassengerException.class)
	/*
	 * Testing the queuePassenger() method to make sure it throws an exception
	 * when used on a queued passenger
	 */
	public void queuePassengerQueuedPassenger() throws PassengerException {
		firstPassenger.queuePassenger(QUEUE_TIME, DEPARTURE_TIME);
		firstPassenger.queuePassenger(QUEUE_TIME, DEPARTURE_TIME);
	}
	
	@Test (expected = PassengerException.class)
	/*
	 * Testing the queuePassenger() method to make sure it throws an exception
	 * when used on a confirmed passenger
	 */
	public void queuePassengerConfirmedPassenger() throws PassengerException {
		firstPassenger.confirmSeat(CONFIRMATION_TIME, DEPARTURE_TIME);
		firstPassenger.queuePassenger(QUEUE_TIME, DEPARTURE_TIME);
	}
	
	@Test (expected = PassengerException.class)
	/*
	 * Testing the queuePassenger() method to make sure it throws an exception
	 * when used on a refused passenger
	 */
	public void queuePassengerRefusedPassenger() throws PassengerException {
		firstPassenger.refusePassenger(REFUSAL_TIME);
		firstPassenger.queuePassenger(QUEUE_TIME, DEPARTURE_TIME);
	}
	
	@Test (expected = PassengerException.class)
	/*
	 * Testing the queuePassenger() method to make sure it throws an exception
	 * when used on a flown passenger
	 */
	public void queuePassengerFlownPassenger() throws PassengerException {
		firstPassenger.confirmSeat(CONFIRMATION_TIME, DEPARTURE_TIME);
		firstPassenger.flyPassenger(DEPARTURE_TIME);
		firstPassenger.queuePassenger(QUEUE_TIME, DEPARTURE_TIME);
	}
	
	@Test (expected = PassengerException.class)
	/*
	 * Testing the queuePassenger() method to make sure it throws an exception
	 * when entered queue time is below zero
	 */
	public void queuePassengerInvalidQueueTimeIsNegative() throws PassengerException {
		firstPassenger.queuePassenger(-6, DEPARTURE_TIME);
	}
	
	@Test (expected = PassengerException.class)
	/*
	 * Testing the queuePassenger() method to make sure it throws an exception
	 * when departureTime is less than queueTime
	 */
	public void queuePassengerDepartureTimeLessThanQueueTime() throws PassengerException {
		firstPassenger.queuePassenger(QUEUE_TIME, 2);
	}
	
	////////////// refusePassenger() Tests /////////////////////
	
	@Test 
	/*
	 * Testing the refusePassenger() method to make sure it successfully 
	 * transfers a new passenger to a refused passenger
	 */
	public void refusePassengerNewPassenger() throws PassengerException {
		firstPassenger.refusePassenger(REFUSAL_TIME);
		assertTrue(firstPassenger.isRefused());
	}
	
	@Test 
	/*
	 * Testing the refusePassenger() method to make sure it successfully 
	 * transfers a queued passenger to a refused passenger
	 */
	public void refusePassengerQueuedPassenger() throws PassengerException {
		firstPassenger.queuePassenger(QUEUE_TIME, DEPARTURE_TIME);
		firstPassenger.refusePassenger(REFUSAL_TIME);
		assertTrue(firstPassenger.isRefused());
	}
	
	@Test (expected = PassengerException.class)
	/*
	 * Testing the refusePassenger() method to make sure it throws an exception
	 * when passenger is already confirmed
	 */
	public void refusePassengerConfirmedPassenger() throws PassengerException {
		firstPassenger.confirmSeat(CONFIRMATION_TIME, DEPARTURE_TIME);
		firstPassenger.refusePassenger(REFUSAL_TIME);
	}
	
	@Test (expected = PassengerException.class)
	/*
	 * Testing the refusePassenger() method to make sure it throws an exception
	 * when passenger has already been refused
	 */
	public void refusePassengerRefusedPassenger() throws PassengerException {
		firstPassenger.refusePassenger(REFUSAL_TIME);
		firstPassenger.refusePassenger(REFUSAL_TIME);
	}
	
	@Test (expected = PassengerException.class)
	/*
	 * Testing the refusePassenger() method to make sure it throws an exception
	 * when passenger has already flown
	 */
	public void refusePassengerFlownPassenger() throws PassengerException {
		firstPassenger.confirmSeat(CONFIRMATION_TIME, DEPARTURE_TIME);
		firstPassenger.flyPassenger(DEPARTURE_TIME);
		firstPassenger.refusePassenger(REFUSAL_TIME);
	}
	
	@Test (expected = PassengerException.class)
	/*
	 * Testing the refusePassenger() method to make sure it throws an exception
	 * when refusalTime is below zero
	 */
	public void refusePassengerInvalidNegativeRefusalTime() throws PassengerException {
		firstPassenger.refusePassenger(-4);
	}
	
	@Test (expected = PassengerException.class)
	/*
	 * Testing the refurePassenger() method to make sure it throws an exception
	 * when refusalTime is less than bookingTime
	 */
	public void refusePassengerRefusalTimeLessThanBookingTime() throws PassengerException {
		firstPassenger.refusePassenger(1);
	}
	
	////////////////// upgradePassenger() Tests ///////////////////
	
	@Test
	/*
	 * Testing the upgrade() method as it is applied to a first class passenger
	 */
	public void upgradeFirstClass() {
		String passID_Before = firstPassenger.getPassID();
		Passenger upgradedPassenger = firstPassenger.upgrade();
		assertTrue(passID_Before == upgradedPassenger.getPassID());
	}
	
	@Test
	/*
	 * Testing the upgrade() method as it is applied to a business class passenger
	 */
	public void upgradeBusinessClass() {
		Passenger upgradedPassenger = businessPassenger.upgrade();
		assertTrue(upgradedPassenger.getPassID().substring(0, 1).equals("F"));
	}
	
	@Test
	/*
	 * Testing the upgrade() method as it is applied to a premium class passenger
	 */
	public void upgradePremiumClass() {
		Passenger upgradedPassenger = premiumPassenger.upgrade();
		assertTrue(upgradedPassenger.getPassID().substring(0, 1).equals("J"));
	}
	
	@Test
	/*
	 * Testing the upgrade() method as it is applied to a economy class passenger
	 */
	public void upgradeEconomyClass() {
		Passenger upgradedPassenger = economyPassenger.upgrade();
		assertTrue(upgradedPassenger.getPassID().substring(0, 1).equals("P"));
	}
	
	/////////////// wasConfirmed() Tests //////////////
	
	@Test
	/*
	 * Testing the wasConfirmed() method as applied to a passenger
	 * that has previously been confirmed
	 */
	public void wasConfirmedIsTrue() throws PassengerException {
		firstPassenger.confirmSeat(CONFIRMATION_TIME, DEPARTURE_TIME);
		assertTrue(firstPassenger.wasConfirmed());
	}
	
	@Test
	/*
	 * Testing the wasConfirmed() method as applied to a passenger
	 * that has not previously been confirmed
	 */
	public void wasConfirmedIsFalse() throws PassengerException {
		assertFalse(firstPassenger.wasConfirmed());
	}
	
	///////////////// wasConfirmed() Tests ////////////////
	
	@Test
	/*
	 * Testing the wasQueued() method as applied to a passenger
	 * that has previously been placed in a queue
	 */
	public void wasQueuedIsTrue() throws PassengerException {
		firstPassenger.queuePassenger(QUEUE_TIME, DEPARTURE_TIME);
		assertTrue(firstPassenger.wasQueued());
	}
	
	@Test
	/*
	 * Testing the wasQueued() method as applied to a passenger
	 * that has not previously been placed on a queue
	 */
	public void wasQueuedIsFalse() throws PassengerException {
		assertFalse(firstPassenger.wasQueued());
	}
	
	
	

	
	
	
	

}
